"""
 A profile for traning tensorflow neural networks and connecting datasets containing large amounts of training data
 The sytem boots on ubuntu 20.04 and uses tensorflow version 
"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum

# Define some pc configurations
PC_TYPE = "d740"
USE_GPU = True
PC_IMG = "urn:publicid:IDN+lab.cityofthings.eu+image+emulab-ops:UBUNTU18-64-STD"
STARTUP_SCRIPT = "/local/repository/startup.sh"


NNDS_MNT = "/mnt/nndataset"
NNDS_URN = "urn:publicid:IDN+emulab.net:sigdisccnn+ltdataset+Processed_anechoic_data"
NNDS_TYPES = [("readwrite", "Read-Write (persistent)"),
               ("readonly", "Read Only"),
               ("rwclone", "Read-Write Clone (not persistent)")]

DS_MNT = "/mnt/dataset"
DS_URN = "urn:publicid:IDN+emulab.net:sigdisccnn+ltdataset+Raw_anechoic_data"
DS_TYPES = [("readwrite", "Read-Write (persistent)"),
               ("readonly", "Read Only"),
               ("rwclone", "Read-Write Clone (not persistent)")]


# create a portal context 
pc = portal.Context()


# Define parameters of the profile
pc.defineParameter( "type", "Compute node hardware type",
                    portal.ParameterType.STRING, PC_TYPE, longDescription="Specify what type of computer you want to run profile on" )

pc.defineParameter("gpu", "use a GPU for the setup (Optional)",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("nnds", "Use A Neural Netrok Training Dataset (Optional)",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("ds", "Use an additional Dataset (Optional)",
                   portal.ParameterType.BOOLEAN, False)

pc.defineParameter("nndsurn", "Remote Neural Netwrok Dataset URN",
                   portal.ParameterType.STRING, NNDS_URN, advanced=True)

pc.defineParameter("nndsmp", "Remote Neural Netwrok Dataset Mount Point",
                   portal.ParameterType.STRING, NNDS_MNT, advanced=True,
                   longDescription="Mount point for the training dataset.")

pc.defineParameter("nndstype", "Remote Neural Netwrok Dataset Acess Type",
                   portal.ParameterType.STRING, NNDS_TYPES[1], DS_TYPES, advanced=True)

pc.defineParameter("dsurn", "Remote Dataset URN (Optional)",
                   portal.ParameterType.STRING, DS_URN, advanced=True)

pc.defineParameter("dsmp", "Remote Dataset Mount Point (Optional)",
                   portal.ParameterType.STRING, DS_MNT, advanced=True,
                   longDescription="Mount point for the training dataset.")

pc.defineParameter("dstype", "Remote Dataset Acess Type (Optional)",
                   portal.ParameterType.STRING, DS_TYPES[1], DS_TYPES, advanced=True)

# Bind and verify parameters.
params = pc.bindParameters()
pc.verifyParameters()

# Request
request = pc.makeRequestRSpec()
pc1 = request.RawPC("pc1")
pc1.hardware_type = params.type #select the pc type to use
pc1.disk_image = PC_IMG # select the OS image to use 

pc1.addService(pg.Execute(shell="sh", command=STARTUP_SCRIPT))

# create a blockstore for temporary use in the project
#bs2 = pc1.Blockstore("scratch","/scratch")
#bs2.size = "10GB"
#bs2.placement = "nonsysvol"



##########################################################
# mount the remote datasets
###########################################################
if params.nnds:
    # specify blockstore location for the CNN dataset
    nnbs = request.RemoteBlockstore("CNN_dataset", params.nndsmp)
    if params.nndstype == "rwclone":
        nnbs.rwclone = True
    elif params.nndstype == "readonly":
        nnbs.readonly = True

    # soeciyf the urn to the dataset
    nnbs.dataset = params.nndsurn

    # create a link between the pc and bs
    nnfslink = request.Link("nndslink", members=(pc1, nnbs.interface))
    nnfslink.best_effort = True
    nnfslink.vlan_tagging = True


if params.ds:
    # specify blockstore location for the other generic dataset
    bs = request.RemoteBlockstore("dataset", params.dsmp)
    if params.dstype == "rwclone":
        bs.rwclone = True
    elif params.dstype == "readonly":
        bs.readonly = True

    # soeciyf the urn to the dataset
    bs.dataset = params.dsurn

    # create a link between the pc and bs
    fslink = request.Link("dslink", members=(pc1, bs.interface))
    fslink.best_effort = True
    fslink.vlan_tagging = True

if params.gpu == True:
    pc1.Desire("GPU", 1)


# Finish and make the request
pc.printRequestRSpec()
