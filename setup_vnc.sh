#!/bin/bash

sudo iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -P INPUT DROP
sudo iptables -A FORWARD -i lo -o lo -j ACCEPT
sudo iptables -P FORWARD DROP

sudo ip6tables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
sudo ip6tables -A INPUT -i lo -j ACCEPT
sudo ip6tables -P INPUT DROP
sudo ip6tables -A FORWARD -i lo -o lo -j ACCEPT
sudo ip6tables -P FORWARD DROP

sudo sh -c "iptables-save > /etc/iptables/rules.v4"
sudo sh -c "ip6tables-save > /etc/iptables/rules.v6"

sudo apt-get install -y iptables-persistent
sudo systemctl enable netfilter-persistent.service


# Temporary until next disk image gets built:
sudo apt-get update
sudo apt-get install -y debconf-utils
echo "gdm3 shared/default-x-display-manager select lightdm" | sudo debconf-set-selections
echo "lightdm shared/default-x-display-manager select lightdm" | sudo debconf-set-selections

export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -y x11vnc xfce4 xfce4-terminal

sudo systemctl disable lightdm
sudo systemctl stop lightdm || /bin/true
