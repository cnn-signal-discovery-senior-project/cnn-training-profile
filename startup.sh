#!/bin/bash

#specify parameters
REPO="https://gitlab.flux.utah.edu/cnn-signal-discovery-senior-project/cnn-training-profile.git"




# update the system before continueing
sudo apt-get update 

apt install git
# grab projects submodules
cd /local/repository
git submodule update --remote
git submodule foreach 'git checkout main'

# create a virtual enviorment to store pyton packages
sudo apt-get install -y python3-pip python3-venv

pip3 install virtualenv
python3 -m venv /local/dev
source /local/dev/bin/activate.csh

# install needed python packages from the enviorment
sudo /local/dev/bin/pip install -U pip
/local/dev/bin/pip install -r /local/repository/requirments.txt

/local/dev/bin/pip install --force-reinstall tensorflow

deactivate 

# setup and spin up the vnc for better displaying
# don't include it now as it requiers manual input just have user run it in the program
#bash ./setup_vnc.sh
#bash ./start_vnc.sh



# install some gpu drivers 
sudo source /local/repository/install_cclibs.sh
